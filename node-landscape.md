
# Node.js User Management Landscape

Over the past few years, I have been extremely excited to watch the Node.js community grow. I used it for research purposes in 2010, then built a social sports betting game (RIP) in 2012, then watched a large enterprise build node apps that can be used by millions. More recently I've been helping companies such as Stormpath build out node SDKs. It seems like the sky is the limit for node. 

Since we recently rolled out the first iteration of the [Stormpath Node SDK](https://github.com/stormpath/stormpath-sdk-node), I've found myself thinking a lot about how developers approach user management and authentication in node. 

When gravitating to node to build an app, you need to get a handle on how to do user management and authentication. This blog post aims to give you the lay of the land for user management and authentication in node.   

### What is available in Node?

Node today has several different paths to build user management. In no particular order there are:

+ Passport.js / Everyauth
+ Your Own Database and a Hashing Algorithm
+ User Management as a Service

### Passport.js / Everyauth

<a href="http://passport.js" target="_blank">PassportJS</a> and <a href="https://github.com/bnoguchi/everyauth" target="_blank">Everyauth</a> are  authentication middleware for node that adopts the <a href="http://www.senchalabs.org/connect/" target="_blank">Connect</a> middleware conventions.  That means if you are using a framework like <a href="http://expressjs.com" target="_blank">Express</a>, <a href="http://mcavage.me/node-restify" target="_blank">Restify</a>, or <a href="http://sailsjs.org/" target="_blank">Sails</a> you can easily plug in one of their authentication schemes (or strategies) directly in your application.  This includes everything from a local username/password strategy, to a slew of OpenID and OAuth providers.  Connect will also help you with session management, including:

+ Serialization of the authenticated user
+ Managing the session
+ Logging the user out

But if you aren't using Connect middleware, these frameworks can get complicated. Additionally, these frameworks are designed for simple, easy authentication, but falls short (by design) on the overall user management needs.  

For example, if you are using passport-local (the strategy to authenticate username / password against your own database), user signup and account verification is not handled by Passport.  You will need to work with database modules to authenticate to a database, create the account, track verification status, create a verification token, send an email, and verify the account.  This means that a developer will need to worry about URL safety, removing expired tokens, and the other security constraints (like hashing the password correctly in a database).

If you are looking for something that is similar to passport / everyauth to provide additional user management functionality, there are larger frameworks like  <a href="https://github.com/jedireza/drywall/" target="_blank">Drywall</a>.  

### Your Own Database and a Hashing Algorithm

The do-it-yourself approach doesn't rely on any middleware.  You choose your own stack, a database to store users (likely PostgresSQL and MongoDB) and a hashing algorithm to generate password hashes (likely bcrypt and scrypt).Searching for bcrypt or scrypt in npm will result in quite a few modules, each with their own set's of dependencies. Be particularly careful if you are a Windows developer - we recommend the <a href="https://github.com/shaneGirish/bcrypt-nodejs" target="_blank">native JS implementation of bcrypt</a>.

After deciding on a stack, you will need to build out user management and authentication.  Historically this approach has been extremely common but is tedious, prone to error, and requires more maintenance than other approaches.

You will need to build/figure out:

+ Account Creation
    + Create a user schema to hold user data
    + Create accounts and store salt + hashed passwords using bcrypt / scrypt
    + Sending an email with token for account verification
+ Account Authentication
    + Authenticate the user (comparing hashes)
+ Account Management
    + Password reset work flow
        + Generating / invalidating tokens
    + Role-based access / permissions
+ Secure the system
    + Secure the Database from unauthorized access
    + Secure the OS from unauthorized access
+ Backups for data

When using a hashing algorithm like bcrypt or scrypt,  you can control the computational complexity (cost factor) associated with generating the password hash. This cost factor is a good way to deter brute force attacks. It becomes easier and easier to generate millions of hashes in less time to brute force because computational power as we see Moore's Law in effect. That being said, the cost factor that you set today may introduce a vulernability in the future if your password hashes are exposed. Increasing the cost factor of bcrypt is required over time and will require analysis of your hash complexity on modern hardware.  Just don't set your cost factor and forget it!

### User Management as a Service

Over time, software has moved from on premise, to the cloud, to distributed API services. At the same time, development teams have come to rely on open source software as much as custom code. It is now possible to offload user management to a system that exposes user management functionality via REST APIs and SDKs for application development. [Stormpath](https://stormpath.com), of course, falls into this category. There are other competitors in this space though, such as UserApp.io and Auth0. 

Typically, API-driven services allow for more common functionality around user management, going beyond just authentication.  Some of this functionality includes:

+ Account email verification
+ Password reset work flows
+ Role based access / permissions
+ Custom Key-Value Stores
+ Two-factor authentication
+ Single sign on between applications

This really has the convenience factor. User management as a service reduces the time spent on user management, but there are some things to consider: all API services should be evaluated for transport security, availability / performance, portability and the flexibility of the data model.

Transport security between your application and the API Service is important in this approach compared to the approaches above.  The additional network communication needs to be secured.  For example, Stormpath](https://stormpath.com) supports only HTTPS and uses a custom digest auth algorithm to guard against replay and man in the middle attacks. You can read more about our security [here](https://stormpath.com/resources/security-and-availability/).

[Availability and performance](https://stormpath.com/resources/security-and-availability/ are extremely important because a critical system like user authentication and management can not be offline or slow.  Being based on modern cloud infrastructures is a good start, but it is important that the service is prepared for the worst case scenarios - like whole data centers going down.  If you build on top of a service for user management, make sure you can subscribe to notifications of any outages. 

Portability is also critical - can you move user data in and out in safe (and easy) ways. While its easy to write a script to port unencrypted data over JSON, the ease of porting passwords will heavily depend on how you have stored any pre-existing data.  For example, bcrypt is very portable by design, as it follows Modular Crypt Format.  It is possible for multiple systems and languages to understand how to construct the hash by looking at the hash value itself.

The data model used by authentication services can vary widely - Salesforce, for instance, looks very different from the Stormpath data model, which is based on directories and therefore generic and flexibly. It's worth digging in to make sure that the data model you have planned for your application, is supported by the service. This is particularly true for [mulit-tenant applications or SaaS](https://stormpath.com/multitenant/). 

Additionally, documentation of APIs varies widely - you should make sure your solution outlines what you need before you dive in.

### Building User Management...

...is not an easy task. Inside node there are different solutions, each with a set of pros and cons.  I hope this post helps you get the lay of the land.  If you have any questions, suggestions or experiences that you want to share, feel free to leave a comment or reach out to me on twitter [@omgitstom](https://twitter.com/omgitstom)